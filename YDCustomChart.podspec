#
#  Be sure to run `pod spec lint YDCustomChart.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

    spec.name         = "YDCustomChart"
    spec.version      = "1.0.1"
    spec.summary      = "A YDCustomChart like the Chart"
    spec.description  = "The YDCustomChart is a completely customizable widget that can be used in any iOS app. It also plays a little victory fanfare."
    spec.homepage     = "https://gitlab.com/YDChart/customchart.git"
    spec.license      = "MIT"
    spec.platform     = :ios, "9.0"
    spec.source       = { :git => 'https://gitlab.com/YDChart/customchart.git', :tag => spec.version }
    spec.source_files = "YDCustomChart"
    spec.swift_version = "4.2"
    spec.author = { "youdaozhang" => "zhangyoudao66@163.com" }


end
